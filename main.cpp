/*
 * main.cpp
 *
 *  Created on: Aug 17, 2017
 *      Author: enzoevers
 */

#include <vector>
#include <iostream>
#include <cassert>
#include <string>

#include "Net.h"
#include "TrainingData.h"

void showVectorVals(std::string label, std::vector<double> &vector)
{
	std::cout << label << " ";
	for(unsigned int i = 0; i < vector.size(); i++)
	{
		std::cout << vector[i] << " ";
	}

	std::cout << std::endl;
}

int main()
{
	TrainingData trainData("../trainingData.txt");

	// {3, 2, 1} = 3 input neurons
	//			   2 hidden layer neurons (1 hidden layer)
	//			   1 output neuron
	std::vector<unsigned int> topology;
	trainData.getTopology(topology);
	Net myNet(topology);

	std::vector<double> inputVals, targetVals, resultVals;
	int trainingPass = 0;

	while(!trainData.isEof())
	{
		trainingPass++;
		std::cout << std::endl << "Pass " << trainingPass;

		// Get new input data and feed it forward:
		if(trainData.getNextInputs(inputVals) != topology[0])
		{
			break;
		}
		showVectorVals(": Inputs:", inputVals);
		myNet.feedForward(inputVals);

		// Collect the net's actual results:
		myNet.getResults(resultVals);
		showVectorVals(": Outputs:", resultVals);

		// Train the net what the outputs should have been:
		trainData.getTargetOutputs(targetVals);
		showVectorVals("Targets:", targetVals);
		assert(targetVals.size() == topology.back());

		myNet.backProp(targetVals);

		// Report how well the training is working, average over recent samples:
		std::cout << "Net recent average error: "
				<< myNet.getRecentAverageError() << std::endl;
	}

	std::cout << std::endl << "Done" << std::endl;
	return 0;
}
