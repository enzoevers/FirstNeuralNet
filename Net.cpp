/*
 * Net.cpp
 *
 *  Created on: Aug 17, 2017
 *      Author: enzoevers
 */

#include <iostream>
#include <cassert>
#include <cmath>

#include "Net.h"

Net::Net(const std::vector<unsigned int> &topology)
{
	m_error = 0.0;
	m_recentAverageError = 0.0;
	m_recentAverageSmoothingFactor = 0.0;

	unsigned int numLayers = topology.size();

	for(unsigned int currentLayer = 0; currentLayer < numLayers; currentLayer++)
	{
		// Make a new vector (typedefed to "Layer").
		m_layers.push_back(Layer());
		unsigned int numOutputs = (currentLayer == topology.size()-1) ? 0 : topology[currentLayer + 1];

		// Add topology[currentLayerNum] new objects of the Neuron class to the Layer vector.
		// <= is because we add a bias neuron to each layer.
		for(unsigned int currentNeuron = 0; currentNeuron <= topology[currentLayer]; currentNeuron++)
		{
			m_layers.back().push_back(Neuron(numOutputs, currentNeuron));
		}

		// Set the bias neuron to a value of 1.0
		m_layers.back().back().setOutputVal(1.0);
	}
}

void Net::feedForward(const std::vector<double> &inputVals)
{
	assert(inputVals.size() == m_layers[0].size()-1); // -1 for the bias neuron

	// Assign (latch) the input valus into the input neurons
	for(unsigned int i = 0; i < inputVals.size(); i++)
	{
		m_layers[0][i].setOutputVal(inputVals[i]);
	}

	// Forward propagation
	for(unsigned int currentLayer = 1;currentLayer < m_layers.size(); currentLayer++)
	{
		Layer &prevLayer = m_layers[currentLayer-1];
		// -1 for the bias neuron
		for(unsigned int currentNeuron = 0; currentNeuron < m_layers[currentLayer].size()-1; currentNeuron++)
		{
			m_layers[currentLayer][currentNeuron].feedForward(prevLayer);
		}
	}
}

void Net::backProp(const std::vector<double> &targetVals)
{
	// Calculate overall net error (RMS (Root Mean Square Error) of output neuron errors)

	Layer &outputLayer = m_layers.back();
	m_error = 0.0;

	for(unsigned int currentNeuron = 0; currentNeuron < outputLayer.size() -1; currentNeuron++)
	{
		double delta = targetVals[currentNeuron] - outputLayer[currentNeuron].getOutputVal();
		m_error += delta * delta;
	}
	m_error /= outputLayer.size() -1; // Get average error
	m_error = sqrt(m_error); // Get RMS

	// Implement a recent average measurement:

	m_recentAverageError =
			(m_recentAverageError * m_recentAverageSmoothingFactor + m_error)
			/ (m_recentAverageSmoothingFactor + 1);

	// Calculate output layer gradient

	for(unsigned int currentNeuron = 0; currentNeuron < outputLayer.size()-1; ++currentNeuron)
	{
		outputLayer[currentNeuron].calcOutputGradients(targetVals[currentNeuron]);
	}

	// Calculate gradients in hidden layers

	// Go from right to left on the hidden layers
	for(unsigned int currentLayer = m_layers.size() - 2; currentLayer > 0; currentLayer--)
	{
		Layer &hiddenLayer = m_layers[currentLayer];
		Layer &nextLayer = m_layers[currentLayer + 1];

		for(unsigned int currentNeuron = 0; currentNeuron < hiddenLayer.size(); currentNeuron++)
		{
			hiddenLayer[currentNeuron].calcHiddenGradients(nextLayer);
		}
	}

	// For all layers from outputs to first hidden layer,
	// update connection weights

	for(unsigned int currentLayer = m_layers.size() - 1; currentLayer > 0; currentLayer--)
	{
		Layer &layer = m_layers[currentLayer];
		Layer &prevLayer = m_layers[currentLayer - 1];

		for(unsigned int currentNeuron = 0; currentNeuron < layer.size() - 1; currentNeuron++)
		{
			layer[currentNeuron].updateInputWeights(prevLayer);
		}
	}
}

void Net::getResults(std::vector<double> &resultVals) const
{
	resultVals.clear();

	for(unsigned int currentNeuron = 0; currentNeuron < m_layers.back().size() -1; currentNeuron++)
	{
		resultVals.push_back(m_layers.back()[currentNeuron].getOutputVal());
	}
}
