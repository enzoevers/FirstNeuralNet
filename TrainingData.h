/*
 * TraininData.h
 *
 *  Created on: Aug 17, 2017
 *      Author: enzoevers
 */

#ifndef TRAININGDATA_H_
#define TRAININGDATA_H_

#include <vector>
#include <string>
#include <fstream>
#include <sstream>

class TrainingData
{
public:
	TrainingData(const std::string filename);
	bool isEof() {return m_trainingDataFile.eof(); }
	void getTopology(std::vector<unsigned int> &topology);

	// Return the number of input values read from the file:
	unsigned int getNextInputs(std::vector<double> &inputVals);
	unsigned int getTargetOutputs(std::vector<double> &targetOutputVals);

private:
	std::ifstream m_trainingDataFile;
};


#endif /* TRAININGDATA_H_ */
