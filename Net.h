/*
 * Net.h
 *
 *  Created on: Aug 17, 2017
 *      Author: enzoevers
 */

#ifndef NET_H_
#define NET_H_


#include <vector>

#include "Neuron.h"
#include "Types.h"


class Net
{
public:
	Net(const std::vector<unsigned int> &topology);

	void feedForward(const std::vector<double> &inputVals);
	void backProp(const std::vector<double> &targetVals);
	void getResults(std::vector<double> &resultVals) const;

	double getRecentAverageError() const {return m_recentAverageError;}

private:
	std::vector<Layer> m_layers; // m_layers[layerNum][neuronNum]
	double m_error;
	double m_recentAverageError;
	double m_recentAverageSmoothingFactor;
};


#endif /* NET_H_ */
