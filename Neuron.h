/*
 * Neuron.h
 *
 *  Created on: Aug 17, 2017
 *      Author: enzoevers
 */

#ifndef NEURON_H_
#define NEURON_H_


#include <vector>
#include <cstdlib>

#include "Types.h"

struct Connection
{
	double weight;
	double deltaWeight;
};

class Neuron
{
public:
	Neuron(unsigned int numOutputs, unsigned int my_index);

	void feedForward(const Layer &prevLayer);
	void calcOutputGradients(double targetVal);
	void calcHiddenGradients(const Layer &nextLayer);
	void updateInputWeights(Layer &prevLayer);

	void setOutputVal(double val) {m_outputVal = val;}
	double getOutputVal() const {return m_outputVal;}

private:
	static double eta;		// [0.0 .. 1.0] overall net training rate
	static double alpha;	// [0.0 .. n] multiplier of last weight change (momentum)
	unsigned int m_myIndex;

	double m_outputVal;
	double m_gradient;
	std::vector<Connection> m_outputWeights;

	static double randomWeight() {return std::rand() / double(RAND_MAX);}
	static double transferFunction(double x);
	static double transferFunctionDerivative(double x);
	double sumDOW(const Layer &nextLayer) const;
};

#endif /* NEURON_H_ */
