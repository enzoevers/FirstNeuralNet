/*
 * Neuron.cpp
 *
 *  Created on: Aug 17, 2017
 *      Author: enzoevers
 */

#include <cmath>

#include "Neuron.h"

double Neuron::eta = 0.15;
double Neuron::alpha = 0.5;

Neuron::Neuron(unsigned int numOutputs, unsigned int m_myIndex)
{
	for(unsigned int connection = 0; connection < numOutputs; connection++)
	{
		m_outputWeights.push_back(Connection());
		m_outputWeights.back().weight = randomWeight();
	}

	this->m_myIndex = m_myIndex;
}

void Neuron::feedForward(const Layer &prevLayer)
{
	double sum = 0.0;

	// Sum the output from the prevLayer after multiplying it with the weights
	for(unsigned int currentNeuron = 0; currentNeuron < prevLayer.size(); currentNeuron++)
	{
		sum += prevLayer[currentNeuron].getOutputVal() *
				prevLayer[currentNeuron].m_outputWeights[m_myIndex].weight;
	}

	m_outputVal = Neuron::transferFunction(sum);
}

void Neuron::calcOutputGradients(double targetVal)
{
	double delta = targetVal - m_outputVal;
	m_gradient = delta * Neuron::transferFunctionDerivative(m_outputVal);
}

void Neuron::calcHiddenGradients(const Layer &nextLayer)
{
	double dow = sumDOW(nextLayer);
	m_gradient = dow * Neuron::transferFunctionDerivative(m_outputVal);
}

void Neuron::updateInputWeights(Layer &prevLayer)
{
	// The weights to be updated are in the Connection container
	// in the neurons in the preceding layer

	for(unsigned int currentNeuron = 0; currentNeuron < prevLayer.size(); currentNeuron++)
	{
		Neuron &neuron = prevLayer[currentNeuron];
		double oldDeltaWeight = neuron.m_outputWeights[m_myIndex].deltaWeight;

		double newDeltaWeight =
				// Individual input, magnified by the gradient and train rate:
				eta
				* neuron.getOutputVal()
				* m_gradient
				// Also add momentum = a fraction if the previous delta weight
				+ alpha
				* oldDeltaWeight;

		neuron.m_outputWeights[m_myIndex].deltaWeight = newDeltaWeight;
		neuron.m_outputWeights[m_myIndex].weight += newDeltaWeight;
	}
}

double Neuron::transferFunction(double x)
{
	return std::tanh(x);
}


double Neuron::transferFunctionDerivative(double x)
{
	return 1 - x*x;
}

double Neuron::sumDOW(const Layer &nextLayer) const
{
	double sum = 0.0;

	// Sum out contributions of the errors at the nodes we feed

	for(unsigned int currentNeuron = 0; currentNeuron < nextLayer.size() - 1; currentNeuron++)
	{
		sum += m_outputWeights[currentNeuron].weight * nextLayer[currentNeuron].m_gradient;
	}

	return sum;
}
