/*
 * Types.h
 *
 *  Created on: Aug 17, 2017
 *      Author: enzoevers
 */

#ifndef TYPES_H_
#define TYPES_H_


#include <vector>

#include "Neuron.h"

class Neuron;
typedef std::vector<Neuron> Layer;


#endif /* TYPES_H_ */
